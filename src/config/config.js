var config = {
    RESOURCE_TYPE : process.env.VUE_APP_DEFAULT_TYPE,
    USER_GROUP_ID: process.env.VUE_APP_DEFAULT_USER_GROUP_ID,
    API_KEY: process.env.VUE_APP_ZOTERO_API_KEY,

}

module.exports = config
