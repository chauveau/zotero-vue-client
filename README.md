# zotero-vue-client

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

## component usage example

```
<zotero-vue-client default-type="user" default-id="1234" @entry-selected="selectedEntryCallback"/>
```

where **default-type** is *"user"* or *"group"* and **default-id** is a *group* or *user* id (https://www.zotero.org/support/dev/web_api/v3/basics)

Another example with custom options :

```
<zotero-vue-client
    default-type="user"
    default-id="475425"
    default-query="1984"
    :options="{
        pageLength: 10,
        editableConfig: false
      }"
    @entry-selected="selectedEntryCallback"/>
```
`selectedEntryCallback` will received the selected entry as argument (in JSON format)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
